package com.example.demo.example;

import com.example.demo.example.service.HealthService;

public class GymCoach implements Coach {

    private HealthService healthService;

    public GymCoach(HealthService healthService){
        this.healthService = healthService;
    }

    @Override
    public String getDailyWorkout() {
        return "Daily workout from Gym Coach";
    }

    @Override
    public String getHealthyDiet() {
        return healthService.getHealthyDiet();
    }
}
