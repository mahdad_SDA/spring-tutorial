package com.example.demo.example;

public interface Coach {
    String getDailyWorkout();
    String getHealthyDiet();
}
