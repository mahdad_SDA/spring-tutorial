package com.example.demo.example;

import com.example.demo.example.service.HealthService;

public class SwimCoach implements Coach {

    private HealthService healthService;

    public void setHealthService(HealthService healthService){
        this.healthService = healthService;
    }

    @Override
    public String getDailyWorkout() {
        return "Daily Workout from Swim Coach";
    }

    @Override
    public String getHealthyDiet() {
        return healthService.getHealthyDiet();
    }

}
