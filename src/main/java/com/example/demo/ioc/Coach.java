package com.example.demo.ioc;

public interface Coach {

	public String getDailyWorkout();
	
}
