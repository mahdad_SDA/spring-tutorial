package com.example.demo.ioc;

import com.example.demo.di.by_constructor.Coach;
import com.example.demo.di.by_constructor.service.DietService;
import com.example.demo.di.by_constructor.service.impl.DietServiceImpl;

public class BasketballCoach implements Coach {

    private DietService dietService;

    public BasketballCoach(DietServiceImpl dietService) {
        this.dietService = dietService;
    }

    @Override
    public String getDailyWorkout() {
        return "Daily workout from Basketball";
    }

    @Override
    public String getDailyDietPlan() {
        return dietService.getDietPlan();
    }
}
