package com.example.demo.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HelloSpringApp {

	public static void main(String[] args) {

		// load the spring configuration file
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
				
		// retrieve bean from spring container
		Coach theCoach = context.getBean("lampard", Coach.class);
		theCoach.getDailyWorkout();

//        Coach theCoach2 = context.getBean("myCoach", Coach.class);
//        Coach mybaseballCoach = context.getBean("myBaseballCoach", Coach.class);
		// call methods on the bean
//        System.out.println("are equal? :"+(theCoach==theCoach2));
		System.out.println(theCoach.getDailyWorkout());
//         System.out.println(mybaseballCoach.getDailyWorkout());
		
		// close the context
		context.close();
	}

}







