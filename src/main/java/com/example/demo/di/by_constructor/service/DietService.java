package com.example.demo.di.by_constructor.service;

public interface DietService {
     String getDietPlan();
}
