package com.example.demo.di.by_constructor;

public interface Coach {

	public String getDailyWorkout();
    public String getDailyDietPlan();
	
}
