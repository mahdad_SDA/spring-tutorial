package com.example.demo.di.by_constructor.service.impl;

import com.example.demo.di.by_constructor.service.DietService;

public class DietServiceImpl implements DietService {

    @Override
    public String getDietPlan() {
        return "Eat whatever you like life is too short ;)";
    }

}
