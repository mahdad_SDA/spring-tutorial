package com.example.demo.di.by_constructor;

import com.example.demo.di.by_constructor.service.DietService;

public class BaseballCoach implements Coach {

    private DietService dietService;

    public BaseballCoach(DietService theDietService) {
        dietService = theDietService;
    }


    @Override
	public String getDailyWorkout() {
	    return "Spend 30 minutes on batting practice";
	}

	@Override
    public String getDailyDietPlan(){
        return dietService.getDietPlan();
    }

}








