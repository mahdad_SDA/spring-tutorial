package com.example.demo.di.by_setter.service.impl;

import com.example.demo.di.by_setter.service.DietService;

public class DietServiceImpl implements DietService {
    @Override
    public String getDietPlan() {
        return "Eat whatever you like, life is too short ;)";
    }
}
