package com.example.demo.di.by_setter.service;

public interface DietService {
    public String getDietPlan();
}
