package com.example.demo.di.by_setter;

public interface Coach {

	public String getDailyWorkout();
    public String getDailyDietPlan();
	
}
