package com.example.demo.di.by_setter;

import com.example.demo.di.by_setter.service.DietService;

public class BaseballCoach implements Coach {

    private DietService dietService;

    public void setDietService(DietService dietService) {
        this.dietService = dietService;
    }

    @Override
	public String getDailyWorkout() {
	    return "Spend 30 minutes on batting practice";
	}

	@Override
    public String getDailyDietPlan(){
        return dietService.getDietPlan();
    }

}








